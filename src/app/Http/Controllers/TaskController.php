<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;


class TaskController extends Controller
{
    public function index(){

    }

    public function create(){
        return view('tasks.create');
    }

    public function store(Request $request){
        $validatedData = $request->validate([
            'user_id' => 'integer|required|exists:users,id',
            'title' => 'string|required|max:50',
            'description' => 'string|required|max:255',
        ]);

        $task = Task::create($validatedData);
        return redirect('dashboard');
    }

    public function show(Task $task){
        
    }

    public function edit(Task $task){
        
    }

    public function update(Request $request, Task $task){
        
    }

    public function destroy(int $id){
        $task = Task::find($id);

        $task->delete();
        return redirect('dashboard');
    }

}
