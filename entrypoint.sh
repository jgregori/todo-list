#!/bin/sh

# Run Composer install
composer install --no-dev --prefer-dist --no-interaction --optimize-autoloader

# Agrega los permisos necesarios en las carpetas y además agrega al usuario www-data
chown -R www-data:www-data /var/www/storage /var/www/bootstrap/cache

# Ejecuta las migraciones
php artisan migrate

# Arranca el php-fpm
php-fpm